#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" 
    contest.sites
    ~~~~~~~~~~~~~

    Automatic parsing of problem sites.

    :copyright: (c) 2013 by Andreas Argelius
    :license: BSD
"""

import re
import requests

from BeautifulSoup import BeautifulSoup

class SiteMeta(type):
    patterns = {}
    def __new__(cls, name, par, dct):
        if not name is 'Site' and dct.has_key('pattern'):
            cls.patterns[name] = re.compile(dct['pattern'])
        def __init__(self, url):
            self.url=url
            for name, pattern in cls.patterns.items():
                if pattern.search(url):
                    self.__class__ = eval(name)
                    break
        ret = type.__new__(cls, name, par, dct)
        ret.__init__ = __init__
        return ret 

class Site(object):
    __metaclass__ = SiteMeta

    @property
    def html(self):
        try:
            return BeautifulSoup(requests.get(self.url, timeout=1.0).content)
        except:
            return BeautifulSoup()

    @property
    def problem_text(self):
        return None

    @property
    def inputs(self):
        return []

    @property
    def outputs(self):
        return []

class Kattis(Site):
    pattern = r'www\.kattis\.com/problems/.*'

    @property
    def problem_text(self):
        bdy = self.html.find("div", {"class": "problembody"})
        return u'\n'.join(map(unicode, bdy.findAll('p')))
    
    @property
    def inputs(self):
        inputs = []
        tables = self.html.findAll("table", {"class": "sample"})
        for table in tables:
            inputs.append(''.join(table.find("pre").contents).strip())
        return inputs
    
    @property
    def outputs(self):
        outputs = []
        tables = self.html.findAll("table", {"class": "sample"})
        for table in tables:
            outputs.append(''.join(table.findAll("pre")[1].contents).strip())
        return outputs

class Spoj(Site):
    pattern = r'www.spoj.com\/problems\/.*'

    @property
    def problem_text(self):
        start = self.html.find("p", {"align": "justify"})
        ret = ""
        while start.nextSibling:
            s = unicode(start)
            if '<h3>Example</h3>' in s:
                ret+=s[:s.find('<h3>Example</h3>')]+u'</p>'
                break
            else:
                ret+=s
            start = start.nextSibling
        return ret

    @property 
    def inputs(self):
        return [u'Parsing SPOJ.com is hard! Please help.']

    @property 
    def outputs(self):
        return [u'No output. :(']

class ProblemMixin(object):
    @property
    def site(self):
        if not '_site' in self.__dict__:
            self._site = Site(self.url)
        return self._site

    @property
    def problem_text(self):
        return self.site.problem_text

    @property
    def inputs(self):
        return self.site.inputs

    @property
    def outputs(self):
        return self.site.outputs

if __name__ == "__main__":
    s = Site('https://www.kattis.com/problems/vegetables')
    print s.problem_text
    print s.inputs
    print s.outputs
     
    s2 = Site('http://www.spoj.com/problems/CMEXPR/')
    print s2.problem_text
    print s2.inputs
    print s2.outputs

    s3 = Site('http://www.spoj.com/problems/PALIN/')
    print s3.problem_text
    print s3.inputs
    print s3.outputs
