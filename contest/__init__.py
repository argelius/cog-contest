#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" 
    contest
    ~~~~~~~

    Programming contest tracking system.

    :copyright: (c) 2013 by Andreas Argelius
    :license: BSD
"""

from flask import Flask, render_template, flash, redirect, request, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager, login_user, logout_user, UserMixin, login_required, current_user
from flask.ext.wtf import Form
from flask.ext.cache import Cache

from wtforms import validators, TextField, PasswordField, TextAreaField, SelectField
from sqlalchemy import func

from datetime import datetime

import bcrypt
import os

from sites import ProblemMixin

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('HEROKU_POSTGRESQL_CYAN_URL', 'sqlite:////tmp/contest.db')
app.config['SECRET_KEY'] = 'cog123'
app.config['CACHE_TYPE'] = 'simple'
app.config['DEBUG'] = True

db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)

cache = Cache(app)

LANGUAGES = {
    'py': 'Python',
    'c': 'C',
    'cpp': 'C++',
    'java': 'Java',
    'cs': 'C#'
}

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120))
    name = db.Column(db.String(80))
    password = db.Column(db.String(80))
    challenges_solved = db.Column(db.Integer, nullable=False, default=0)
    is_king = db.Column(db.Boolean, nullable=False, default=False)

    def __str__(self):
        return self.name or self.email

    def __init__(self, email, password, name=None):
        self.email = email
        self.password = bcrypt.hashpw(password, bcrypt.gensalt())
        self.name = name

    def check_password(self, password):
        return bcrypt.checkpw(password, self.password)

    def set_password(self, password):
        self.password = bcrypt.hashpw(password, bcrypt.gensalt())

    @property
    def points(self):
        return self.solutions.count()+10*self.challenges_solved

@login_manager.user_loader
def load_user(id):
    return User.query.get(id)

class LoginForm(Form):
    email = TextField('Email', validators=[validators.Email()])
    password = PasswordField('Password')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter(User.email.ilike(form.email.data)).first()
        if not user:
            flash(u'No such user.')
        elif not user.check_password(form.password.data):
            flash(u'Password incorrect.')
        else:
            login_user(user)
            flash(u'User logged in.')
            return redirect(request.args.get("next") or url_for("index"))
    return render_template("login.html", form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))

class ChangePasswordForm(Form):
    old = PasswordField('Old password')
    new = PasswordField('New password', validators=[validators.Length(6)])
    confirm = PasswordField('Confirm new password', validators=[validators.EqualTo('new')])

@app.route('/password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if not current_user.check_password(form.old.data):
            flash(u'Password incorrect.')
        else:
            current_user.set_password(form.new.data)
            db.session.commit()
            flash(u'Password changed. Write it down somewhere so you don\'t forget it!')
    return render_template('change_password.html', form=form)

class Problem(db.Model, ProblemMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    url = db.Column(db.String(500))
    time = db.Column(db.DateTime)
    is_challenge = db.Column(db.Boolean, nullable=False, default=False)
    was_challenge = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, **kwargs):
        super(Problem, self).__init__(**kwargs)
        self.time = datetime.utcnow()
    @property
    def times_solved(self):
        query = Solution.query
        query = query.filter(Solution.problem_id == self.id)
        return query.count()


class AddEditProblemForm(Form):
    name = TextField('Problem name', validators=[validators.Length(2)])
    url = TextField('Link', validators=[validators.URL()])

@app.route('/problems/<int:problem_id>')
@login_required
@cache.cached(timeout=3600)
def problem(problem_id):
    problem = Problem.query.get_or_404(problem_id)
    return render_template("problem.html", problem=problem)

@app.route('/problems/add', methods=['GET', 'POST'])
@login_required
def add_problem():
    form = AddEditProblemForm()
    if form.validate_on_submit():
        problem = Problem.query.filter(Problem.url.like(form.url.data)).first()
        if problem:
            flash(u'Problem already added. View it <a href="{url}">here</a>.'.format(url=url_for('problem', problem_id=problem.id)))
        else:
            problem = Problem()
            form.populate_obj(problem)
            db.session.add(problem)
            db.session.commit()
            flash(u'Problem added')
            return redirect(url_for('problem', problem_id=problem.id))
    return render_template("add_problem.html", form=form)

@app.route('/problems/<int:problem_id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_problem(problem_id):
    problem = Problem.query.get_or_404(problem_id)
    form = AddEditProblemForm(obj=problem)
    if form.validate_on_submit():
        form.populate_obj(problem)
        db.session.commit()
        flash(u'Problem updated.')
        return redirect(url_for('problem', problem_id=problem.id))
    return render_template("edit_problem.html", problem=problem, form=form)

@app.route('/problems')
@login_required
def problems():
    problems = Problem.query.all()
    return render_template("problems.html", problems=problems)

class ChooseChallengeForm(Form):
    challenge = SelectField('Next challenge', coerce=int)

@app.route('/challenge/choose', methods=['GET', 'POST'])
@login_required
def choose_challenge():
    if not current_user.is_king:
        flash(u'You must solve a challenge to be able to choose the next one.')
        return redirect('index')
    
    form = ChooseChallengeForm()
    choices = Problem.query.filter(Problem.was_challenge==False).outerjoin(Problem.solutions).group_by(Problem).having(func.count(Solution.id)==0).all()
    form.challenge.choices = [(p.id, p.name) for p in choices]
    if form.validate_on_submit():
        problem = Problem.query.get_or_404(form.challenge.data)

        if problem.solutions.count() > 0 or problem.was_challenge:
            flash(u'Invalid choice.')
        else:
            problem.is_challenge = True
            current_user.is_king = False
            db.session.commit()
            flash(u'Challenge chosen.')
            return redirect(url_for('index'))
    return render_template('choose_challenge.html', form=form)

@app.route('/challenge')
@login_required
def challenge():
    challenge = Problem.query.filter_by(is_challenge=True).first()
    return render_template("challenge.html", challenge=challenge)

@app.route('/')
def index():
    return render_template('index.html')

class Solution(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'))
    user = db.relationship(User, foreign_keys=user_id, backref=db.backref('solutions', lazy='dynamic'))
    problem = db.relationship(Problem, foreign_keys=problem_id, backref=db.backref('solutions', lazy='dynamic'))
    code = db.Column(db.Text)
    language = db.Column(db.String(80))
    time = db.Column(db.DateTime)

    def __init__(self, **kwargs):
        super(Solution, self).__init__(**kwargs)
        self.time = datetime.utcnow()

class AddSolutionForm(Form):
    code = TextAreaField()
    language = SelectField('Programming language', choices=LANGUAGES.items())
    
@app.route('/problems/<int:problem_id>/solutions/add', methods=['GET', 'POST'])
@login_required
def add_solution(problem_id):
    problem = Problem.query.get_or_404(problem_id)
    form = AddSolutionForm()

    if form.validate_on_submit():
        if current_user.solutions.filter_by(problem_id=problem.id).first():
            flash(u'You have already added a solution for this problem. Solve something else!')
        else:
            solution = Solution(user=current_user, problem=problem)
            form.populate_obj(solution)
            db.session.add(solution)
            flash(u'Added solution.')

            if problem.is_challenge:
                problem.is_challenge = False
                problem.was_challenge = True
                current_user.challenges_solved += 1
                current_user.is_king = True
                flash(u'<strong>Congratulations!</strong> You have solved the challenge. You receive 10 points and get to chose the next challenge.')

            db.session.commit()

            return redirect(url_for("solution", problem_id=problem.id, solution_id=solution.id))

    return render_template("add_solution.html", problem=problem, form=form)

@app.route('/problems/<int:problem_id>/solutions/<int:solution_id>')
def solution(problem_id, solution_id):
    solution = Solution.query.get_or_404(solution_id)
    return render_template('solution.html', solution=solution)

@app.route('/ranking')
def ranking():
    users = User.query.all()
    users.sort(key=lambda x: x.points, reverse=True)
    return render_template('ranking.html', users=users)

if __name__ == "__main__":
    from flask.ext.script import Manager

    manager = Manager(app)
    manager.run()
